<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'manushak');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|5fS)m/-Mng++1X4Y?]`c9U>>;htn[7aERZ~:|]4+;^`#vv(LA5}c)iDaSvu{X#-');
define('SECURE_AUTH_KEY',  'stl-L9rSbfWE99 VY++M)7CZv5Ui t[Ogfy]|_d-JsGl&%s&=m)I:=3@/IH)j%nW');
define('LOGGED_IN_KEY',    '(aA*M(5^VModcCVn)N(v/L|:*~*0 Xg|94==1~QaPu)[3,t`%-bJ<iSC7pZvC%|(');
define('NONCE_KEY',        'MvPsK0yBRO#HU|X<JR#]7QS{J6iQi,z7~?XUt^tN=*&[H?!49L1?G+$]k7b>K#G(');
define('AUTH_SALT',        'NF6h)p1Zn#|b}R,)*q~bzV(MgB==E|(ymJU0Ur&-^@$yn~s04W#Z|(y-t1c}tKR+');
define('SECURE_AUTH_SALT', '|%+jW/WU*Ufmzl.iB3rR5D%)r2nOJ;/V;QP_,O}pQ7ftrTQ_J1,<6+l65Y>cD=cC');
define('LOGGED_IN_SALT',   '2+[wYXA^_Vd4H X&LsR;|3LN9HW[=yOdFxz2#Z/D+GY+)of)Ep%a_5fp`;|HM_cF');
define('NONCE_SALT',       '<0PIT8A-hfr5u}R{<X9PSLK|;4r1;e_#]ff(-CCf|`ZdhP6+G7{ZKt|Di|96r.Wn');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
