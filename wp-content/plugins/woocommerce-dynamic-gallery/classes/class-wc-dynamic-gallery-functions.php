<?php
/**
 * WC Dynamic Gallery Functions
 *
 * Table Of Contents
 *
 * reset_products_galleries_activate()
 * add_google_fonts()
 * html2rgb()
 * a3_wp_admin()
 * wc_dynamic_gallery_extension()
 * plugin_extra_links()
 */
class WC_Dynamic_Gallery_Functions
{

	public static function reset_products_galleries_activate() {
		global $wpdb;
		$wpdb->query( "DELETE FROM ".$wpdb->postmeta." WHERE meta_key='_actived_d_gallery' " );
	}

	public static function add_google_fonts() {
		global $wc_dgallery_fonts_face;

		$caption_font = get_option( WOO_DYNAMIC_GALLERY_PREFIX.'caption_font' );

		$navbar_font = get_option( WOO_DYNAMIC_GALLERY_PREFIX.'navbar_font' );

		$google_fonts = array( $caption_font['face'], $navbar_font['face'] );
		$wc_dgallery_fonts_face->generate_google_webfonts( $google_fonts );
	}

	public static function get_no_image_uri() {
		$no_image_uri = apply_filters( 'wc_dg_no_image_uri', WOO_DYNAMIC_GALLERY_JS_URL . '/mygallery/no-image.png' );

		return $no_image_uri;
	}

	public static function get_gallery_ids( $post_id = 0 ) {

		if ( $post_id < 1 ) return array();

		$have_gallery_ids = false;

		// 1.6.1: a3 gallery custom field for Product support for sort and new UI uploader
		$dgallery_ids = get_post_meta( $post_id, '_a3_dgallery', true );
		if ( ! empty( $dgallery_ids ) && '' != trim( $dgallery_ids ) ) {
			$dgallery_ids = explode( ',', $dgallery_ids );
			if ( count( $dgallery_ids ) > 0 ) {
				$have_gallery_ids = true;
			}
		}

		$featured_is_excluded = 1;
		$featured_img_id      = (int) get_post_meta( $post_id, '_thumbnail_id', true );
		if ( ! empty( $featured_img_id ) && $featured_img_id > 0 ) {
			$featured_is_excluded = get_post_meta( $featured_img_id, '_woocommerce_exclude_image', true );
		}

		// Use the default WooCommerce Gallery if don't have a3 dynamic gallery
		if ( ! $have_gallery_ids ) {

			$dgallery_ids = get_post_meta( $post_id, '_product_image_gallery', true );
			if ( ! empty( $dgallery_ids ) && '' != trim( $dgallery_ids ) ) {
				$dgallery_ids = explode( ',', $dgallery_ids );
				if ( 1 != $featured_is_excluded ) {
					$dgallery_ids = array_merge( array( $featured_img_id ), $dgallery_ids );
				}

				if ( count( $dgallery_ids ) > 0 ) {
					$have_gallery_ids = true;
				}
			}

		}

		if ( $have_gallery_ids ) {

			foreach ( $dgallery_ids as $img_id ) {
				// Remove image id if it is not image
				if ( ! wp_attachment_is_image( $img_id ) ) {
					$dgallery_ids = array_diff( $dgallery_ids, array( $img_id ) );
				}
			}

			if ( count( $dgallery_ids ) > 0 ) {
				return $dgallery_ids;
			}

		}

		// set dgallery_ids to empty array
		$dgallery_ids = array();

		if ( 1 != $featured_is_excluded ) {
			$dgallery_ids[]    = $featured_img_id;
		}

		$attached_images      = (array) get_posts( array(
			'post_type'      => 'attachment',
			'post_mime_type' => 'image',
			'numberposts'    => -1,
			'post_status'    => null,
			'post_parent'    => $post_id,
			'orderby'        => 'menu_order',
			'order'          => 'ASC',
			'exclude'        => array( $featured_img_id ),
		) );

		if ( is_array( $attached_images ) && count( $attached_images ) > 0 ) {
			foreach ( $attached_images as $item_thumb ) {
				$is_excluded   = get_post_meta( $item_thumb->ID, '_woocommerce_exclude_image', true );

				// Don't get if this image is excluded on main gallery
				if ( 1 == $is_excluded ) continue;

				$dgallery_ids[]    = $item_thumb->ID;
			}
		}

		return $dgallery_ids;
	}

	public static function html2rgb($color,$text = false){
		if ($color[0] == '#')
			$color = substr($color, 1);

		if (strlen($color) == 6)
			list($r, $g, $b) = array($color[0].$color[1],
									 $color[2].$color[3],
									 $color[4].$color[5]);
		elseif (strlen($color) == 3)
			list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
		else
			return false;

		$r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
		if($text){
			return $r.','.$g.','.$b;
		}else{
			return array($r, $g, $b);
		}
	}

	public static function a3_wp_admin() {
		wp_enqueue_style( 'a3rev-wp-admin-style', WOO_DYNAMIC_GALLERY_CSS_URL . '/a3_wp_admin.css' );
	}

	public static function plugin_extension() {
		$html = '';
		$html .= '<a href="http://a3rev.com/shop/" target="_blank" style="float:right;margin-top:5px; margin-left:10px;" ><div class="a3-plugin-ui-icon a3-plugin-ui-a3-rev-logo"></div></a>';
		$html .= '<h3>'.__('Upgrade to Dynamic Gallery Pro', 'woo_dgallery').'</h3>';
		$html .= '<p>'.__("<strong>NOTE:</strong> All the functions inside the Yellow border on the plugins admin panel are extra functionality that is activated by upgrading to the Pro version", 'woo_dgallery').':</p>';
		
		$html .= '<h3>* '.__('WooCommerce Dynamic Gallery Pro Features', 'woo_dgallery').':</h3>';
		$html .= '<p>';
		$html .= '<ul style="padding-left:10px;">';
		$html .= '<li>1. '.__('Show Multiple Product Variation images in Gallery.', 'woo_dgallery').'</li>';
		$html .= '<li>2. '.__('Assign images to variations and they show when variation is selected.', 'woo_dgallery').'</li>';
		$html .= '<li>3. '.__('Set a single image or set of gallery of images to show for each variation.', 'woo_dgallery').'</li>';
		$html .= '<li>4. '.__('Fully mobile Responsive Gallery option.', 'woo_dgallery').'</li>';
		$html .= '<li>5. '.__('Set gallery wide to % and it becomes fully responsive image product gallery.', 'woo_dgallery').'</li>';
		$html .= '<li>6. '.__('Activate all of the Gallery customization settings.', 'woo_dgallery').'</li>';
		$html .= '<li>7. '.__('Fine tune your product image gallery presentation.', 'woo_dgallery').'</li>';
		$html .= '<li>8. '. sprintf( __('View all Pro features at <a href="%s" target="_blank">a3rev.com</a>.', 'woo_dgallery'), 'http://a3rev.com/shop/woocommerce-dynamic-gallery/' ).'</li>';
		$html .= '</ul>';
		$html .= '</p>';
		$html .= '<h3>'.__('View this plugins', 'woo_dgallery').' <a href="http://docs.a3rev.com/user-guides/woocommerce/woo-dynamic-gallery/" target="_blank">'.__('documentation', 'woo_dgallery').'</a></h3>';
		$html .= '<h3>'.__('Visit this plugins', 'woo_dgallery').' <a href="http://wordpress.org/support/plugin/woocommerce-dynamic-gallery/" target="_blank">'.__('support forum', 'woo_dgallery').'</a></h3>';

		$html .= '<h3>'.__('More a3rev Quality Plugins', 'woo_dgallery').'</h3>';
		$html .= '<p>';
		$html .= '<ul style="padding-left:10px;">';
		$html .= '<li>* <a href="http://wordpress.org/plugins/woocommerce-product-sort-and-display/" target="_blank">'.__('WooCommerce Product Sort & Display', 'woo_dgallery').'</a></li>';
		$html .= '<li>* <a href="http://wordpress.org/plugins/woocommerce-products-quick-view/" target="_blank">'.__('WooCommerce Products Quick View', 'woo_dgallery').'</a></li>';
		$html .= '<li>* <a href="http://wordpress.org/plugins/woocommerce-predictive-search/" target="_blank">'.__('WooCommerce Predictive Search', 'woo_dgallery').'</a></li>';
		$html .= '<li>* <a href="http://wordpress.org/plugins/woocommerce-compare-products/" target="_blank">'.__('WooCommerce Compare Products', 'woo_dgallery').'</a></li>';
		$html .= '<li>* <a href="http://wordpress.org/plugins/woo-widget-product-slideshow/" target="_blank">'.__('WooCommerce Widget Product Slideshow', 'woo_dgallery').'</a></li>';
		$html .= '<li>* <a href="http://wordpress.org/plugins/woocommerce-email-inquiry-cart-options/" target="_blank">'.__('WooCommerce Email Inquiry & Cart Options', 'woo_dgallery').'</a></li>';
		$html .= '</ul>';
		$html .= '</p>';
		$html .= '<h3>'.__('FREE a3rev WordPress Plugins', 'woo_dgallery').'</h3>';
		$html .= '<p>';
		$html .= '<ul style="padding-left:10px;">';
		$html .= '<li>* <a href="https://wordpress.org/plugins/a3-lazy-load/" target="_blank">'.__('a3 Lazy Load', 'woo_dgallery').'</a> ('.__( 'WooCommerce Compatible' , 'woo_dgallery' ).')</li>';
		$html .= '<li>* <a href="https://wordpress.org/plugins/a3-portfolio/" target="_blank">'.__('a3 Portfolio', 'woo_dgallery').'</a></li>';
		$html .= '<li>* <a href="http://wordpress.org/plugins/a3-responsive-slider/" target="_blank">'.__('a3 Responsive Slider', 'woo_dgallery').'</a></li>';
		$html .= '<li>* <a href="http://wordpress.org/plugins/contact-us-page-contact-people/" target="_blank">'.__('Contact Us Page - Contact People', 'woo_dgallery').'</a></li>';
		$html .= '<li>* <a href="http://wordpress.org/plugins/wp-email-template/" target="_blank">'.__('WordPress Email Template', 'woo_dgallery').'</a></li>';
		$html .= '<li>* <a href="http://wordpress.org/plugins/page-views-count/" target="_blank">'.__('Page View Count', 'woo_dgallery').'</a></li>';
		$html .= '</ul>';
		$html .= '</p>';
		return $html;
	}

	public static function plugin_extra_links($links, $plugin_name) {
		if ( $plugin_name != WOO_DYNAMIC_GALLERY_NAME) {
			return $links;
		}
		$links[] = '<a href="http://docs.a3rev.com/user-guides/woocommerce/woo-dynamic-gallery/" target="_blank">'.__('Documentation', 'woo_dgallery').'</a>';
		$links[] = '<a href="http://wordpress.org/support/plugin/woocommerce-dynamic-gallery/" target="_blank">'.__('Support', 'woo_dgallery').'</a>';
		return $links;
	}

	public static function settings_plugin_links($actions) {
		$actions = array_merge( array( 'settings' => '<a href="admin.php?page=woo-dynamic-gallery">' . __( 'Settings', 'woo_dgallery' ) . '</a>' ), $actions );

		return $actions;
	}
}
?>
