<?php
/**
 * WooCommerce Dynamic Gallery Meta_Boxes Class
 *
 * Class Function into woocommerce plugin
 *
 * Table Of Contents
 *
 * woocommerce_meta_boxes_image()
 * woocommerce_product_image_box()
 * save_actived_d_gallery()
 */
class WC_Dynamic_Gallery_Meta_Boxes
{

	public static function woocommerce_meta_boxes_image() {
		add_meta_box( 'wc-dgallery-product-images', __( 'A3 Dynamic Image Gallery', 'woo_dgallery' ), array( 'WC_Dynamic_Gallery_Meta_Boxes','woocommerce_product_image_box' ), 'product', 'normal', 'high' );
	}

	public static function woocommerce_product_image_box() {
		global $post;

		$wc_dgallery_hide_woo_gallery = get_option( WOO_DYNAMIC_GALLERY_PREFIX.'hide_woo_gallery', 'no' );
		$global_wc_dgallery_activate  = get_option( WOO_DYNAMIC_GALLERY_PREFIX.'activate' );
		$actived_d_gallery            = get_post_meta( $post->ID, '_actived_d_gallery',true );

		if ($actived_d_gallery == '' && $global_wc_dgallery_activate != 'no') {
			$actived_d_gallery = 1;
		}

		wp_enqueue_style( 'a3-dynamic-metabox-admin-style' );
		if ( is_rtl() ) {
			wp_enqueue_style( 'a3-dynamic-metabox-admin-style-rtl' );
		}
		wp_enqueue_script( 'a3-dynamic-metabox-admin-script' );
		wp_localize_script( 'a3-dynamic-metabox-admin-script', 'a3_dgallery_metabox', array( 'ajax_url' => admin_url( 'admin-ajax.php', 'relative' ) ) );
		wp_enqueue_media();

		ob_start();

		?>
		<style>
		#woocommerce-product-images {
		<?php if ( $actived_d_gallery == 1 && $wc_dgallery_hide_woo_gallery == 'yes' ) { ?>
			display: none;
		<?php } else { ?>
			display: block;
		<?php } ?>
		}
        </style>
        <div class="a3rev_panel_container a3-metabox-panel-wrap a3-dynamic-metabox-panel-wrap" style="padding-left: 0px;">

			<div style="margin-bottom:10px;">
        		<label class="a3_actived_d_gallery" style="margin-right: 50px;">
        			<input type="checkbox" <?php checked( 1, $actived_d_gallery, true ); ?> value="1" name="actived_d_gallery" class="actived_d_gallery" /> 
        			<?php echo __( 'A3 Dynamic Image Gallery activated', 'woo_dgallery' ); ?>
        		</label>
        	</div>

			<div id="main_dgallery_panel" class="dgallery_images_container a3-metabox-panel a3-metabox-options-panel" style="<?php if ( 1 != $actived_d_gallery ) { echo 'display: none;'; } ?>">

				<ul class="dgallery_images">
					<?php
						$dgallery_ids = WC_Dynamic_Gallery_Functions::get_gallery_ids( $post->ID );
						if ( is_array( $dgallery_ids ) && count( $dgallery_ids ) > 0 ) {
							foreach ( $dgallery_ids as $img_id ) {
								$img_data = wp_get_attachment_image_src( $img_id, 'thumbnail' );
					?>
					<li class="image" data-attachment_id="<?php echo $img_id ; ?>">
						<img class="image_item" src="<?php echo $img_data['0']; ?>" />
						<ul class="actions">
							<li><a href="#" class="delete dg_tips" data-tip="<?php echo __( 'Delete image', 'woo_dgallery' ); ?>"><?php echo __( 'Delete image', 'woo_dgallery' ); ?></a></li>
						</ul>
					</li>
					<?php
							}
						}
					?>
				</ul>

				<input type="hidden" class="dgallery_ids" name="dgallery_ids" value="<?php if ( $dgallery_ids ) echo esc_attr( implode( ',', $dgallery_ids ) ); ?>" />

				<p class="add_dgallery_images hide-if-no-js">
					<a href="#" data-choose="<?php _e( 'Add Images to Dynamic Gallery', 'woo_dgallery' ); ?>" data-update="<?php _e( 'Add to gallery', 'woo_dgallery' ); ?>" data-delete="<?php _e( 'Delete image', 'woo_dgallery' ); ?>" data-text="<?php _e( 'Delete', 'woo_dgallery' ); ?>"><?php _e( 'Add dynamic gallery images', 'woo_dgallery' ); ?></a>
				</p>

			</div>

			<?php
			// Add an nonce field so we can check for it later.
			wp_nonce_field( 'a3_dynamic_metabox_action', 'a3_dynamic_metabox_nonce_field' );
			?>
			<div style="clear: both;"></div>

		</div>
		<div style="clear: both;"></div>

        <script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('input.actived_d_gallery').change(function() {
				if( jQuery(this).is(":checked") ) {
					<?php if ( $wc_dgallery_hide_woo_gallery == 'yes' ) { ?>
					jQuery('#woocommerce-product-images').slideUp();
					<?php } ?>
				} else {
					jQuery('#woocommerce-product-images').slideDown();
				}
			});
		});
		</script>
        <?php
		$output = ob_get_clean();
		echo $output;
	}

	public static function save_actived_d_gallery( $post_id = 0 ) {

		if ( $post_id < 1 ) {
			global $post;
			$post_id = $post->ID;
		}

		// Check if our nonce is set.
		if ( ! isset( $_POST['a3_dynamic_metabox_nonce_field'] ) || ! check_admin_referer( 'a3_dynamic_metabox_action', 'a3_dynamic_metabox_nonce_field' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
		// so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;

		if ( ! current_user_can( 'edit_post', $post_id ) )
			return $post_id;

		if ( 'product' != get_post_type( $post_id ) ) return $post_id;

		if ( isset( $_REQUEST['actived_d_gallery'] ) ) {
			update_post_meta( $post_id, '_actived_d_gallery', 1 );
		} else {
			update_post_meta( $post_id, '_actived_d_gallery', 0 );
		}

		if ( isset( $_POST['dgallery_ids'] ) ) {
			$dgallery_ids = array_filter( explode( ',', trim( $_POST['dgallery_ids'] ) ) );
			update_post_meta( $post_id, '_a3_dgallery', implode( ',', $dgallery_ids ) );
		}
	}
}

add_action( 'add_meta_boxes', array('WC_Dynamic_Gallery_Meta_Boxes','woocommerce_meta_boxes_image'), 9 );
add_action( 'save_post', array('WC_Dynamic_Gallery_Meta_Boxes','save_actived_d_gallery') );
?>
