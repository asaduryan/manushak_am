<?php
/**
 * WooCommerce Gallery Display Class
 *
 * Class Function into woocommerce plugin
 *
 * Table Of Contents
 *
 * wc_dynamic_gallery_display()
 */
class WC_Gallery_Display_Class
{
	public static function frontend_register_scripts() {
		$suffix = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';

		wp_register_style( 'a3-dgallery-style', WOO_DYNAMIC_GALLERY_JS_URL . '/mygallery/jquery.a3-dgallery.css', array(), WOO_DYNAMIC_GALLERY_VERSION );
		wp_register_style( 'woocommerce_fancybox_styles', WOO_DYNAMIC_GALLERY_JS_URL . '/fancybox/fancybox.css', array(), '1.3.4' );
		wp_register_style( 'a3_colorbox_style', WOO_DYNAMIC_GALLERY_JS_URL . '/colorbox/colorbox.css', array(), '1.4.4' );

		wp_register_script( 'a3-dgallery-script', WOO_DYNAMIC_GALLERY_JS_URL . '/mygallery/jquery.a3-dgallery.js', array( 'jquery' ), WOO_DYNAMIC_GALLERY_VERSION, true );
		wp_register_script( 'a3-dgallery-variations-script', WOO_DYNAMIC_GALLERY_JS_URL . '/select_variations.js', array( 'jquery', 'wc-add-to-cart-variation' ), WOO_DYNAMIC_GALLERY_VERSION, true );

		wp_register_script( 'fancybox', WOO_DYNAMIC_GALLERY_JS_URL . '/fancybox/fancybox'.$suffix.'.js', array( 'jquery' ), '1.3.4', true );
		wp_register_script( 'colorbox_script', WOO_DYNAMIC_GALLERY_JS_URL . '/colorbox/jquery.colorbox'.$suffix.'.js', array( 'jquery' ), '1.4.4', true );
	}

	public static function backend_register_scripts() {
		$suffix = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';

		wp_register_style( 'a3-dgallery-style', WOO_DYNAMIC_GALLERY_JS_URL . '/mygallery/jquery.a3-dgallery.css', array( 'thickbox' ), WOO_DYNAMIC_GALLERY_VERSION );
		wp_register_style( 'woocommerce_fancybox_styles', WOO_DYNAMIC_GALLERY_JS_URL . '/fancybox/fancybox.css', array(), '1.3.4' );
		wp_register_style( 'a3_colorbox_style', WOO_DYNAMIC_GALLERY_JS_URL . '/colorbox/colorbox.css', array(), '1.4.4' );
		wp_register_style( 'a3-dynamic-metabox-admin-style', WOO_DYNAMIC_GALLERY_CSS_URL . '/a3.dynamic.metabox.admin.css', array(), WOO_DYNAMIC_GALLERY_VERSION );
		wp_register_style( 'a3-dynamic-metabox-admin-style-rtl', WOO_DYNAMIC_GALLERY_CSS_URL . '/a3.dynamic.metabox.admin.rtl.css', array(), WOO_DYNAMIC_GALLERY_VERSION );

		wp_register_script( 'preview-gallery-script', WOO_DYNAMIC_GALLERY_JS_URL.'/galleries.js', array( 'jquery', 'thickbox' ), WOO_DYNAMIC_GALLERY_VERSION, true );
		wp_register_script( 'a3-dgallery-script', WOO_DYNAMIC_GALLERY_JS_URL . '/mygallery/jquery.a3-dgallery.js', array( 'jquery' ), WOO_DYNAMIC_GALLERY_VERSION, true );

		wp_register_script( 'fancybox', WOO_DYNAMIC_GALLERY_JS_URL . '/fancybox/fancybox'.$suffix.'.js', array( 'jquery' ), '1.3.4', true );
		wp_register_script( 'colorbox_script', WOO_DYNAMIC_GALLERY_JS_URL . '/colorbox/jquery.colorbox'.$suffix.'.js', array( 'jquery' ), '1.4.4', true );

		wp_register_script( 'a3-dynamic-metabox-admin-script', WOO_DYNAMIC_GALLERY_JS_URL . '/a3.dynamic.metabox.admin' . $suffix . '.js', array( 'jquery', 'jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-mouse', 'jquery-ui-sortable' ), WOO_DYNAMIC_GALLERY_VERSION );
	}

	public static function wc_dynamic_gallery_display($product_id=0) {
		/**
		 * Single Product Image
		 */
		global $post, $wc_dgallery_fonts_face;

		$no_image_uri = WC_Dynamic_Gallery_Functions::get_no_image_uri();

		if ( $product_id < 1 ) {
			$product_id = $post->ID;
		}

		// Get gallery of this product
		$dgallery_ids = WC_Dynamic_Gallery_Functions::get_gallery_ids( $product_id );
		if ( ! is_array( $dgallery_ids ) ) {
			$dgallery_ids = array();
		}

		$main_dgallery      = array();
		$ogrinal_product_id = $product_id;
		$product_id         .= '_'.rand(100,10000);
		$lightbox_class     = 'lightbox';

		if ( count( $dgallery_ids ) > 0 ) {

			// Assign image data into main gallery array
			foreach ( $dgallery_ids as $img_id ) {
				// Check if image id is existed on main gallery then just use it again for decrease query
				if ( isset( $main_dgallery[$img_id] ) ) {
					continue;
				}

				$image_data            = get_post( $img_id );
				$large_image_attribute = wp_get_attachment_image_src( $img_id, 'large' );
				$thumb_image_attribute = wp_get_attachment_image_src( $img_id, 'wc-dynamic-gallery-thumb' );

				$alt                   = get_post_meta( $img_id, '_wp_attachment_image_alt', true );
				$img_srcset            = '';
				$img_sizes             = '';

				if ( function_exists( 'wp_get_attachment_image_srcset' ) ) {
					$img_srcset = wp_get_attachment_image_srcset( $img_id, 'wc-dynamic-gallery-thumb' );
				}
				if ( function_exists( 'wp_get_attachment_image_sizes' ) ) {
					$img_sizes = wp_get_attachment_image_sizes( $img_id, 'wc-dynamic-gallery-thumb' );
				}

				$main_dgallery[$img_id] = array (
						'caption_text' => $image_data->post_excerpt,
						'alt_text'     => $alt,
						'img_srcset'   => $img_srcset,
						'img_sizes'    => $img_sizes,
						'thumb'        => array (
								'url'    => $thumb_image_attribute[0],
								'width'  => $thumb_image_attribute[1],
								'height' => $thumb_image_attribute[2]
							),
						'large'        => array (
								'url'    => $large_image_attribute[0],
								'width'  => $large_image_attribute[1],
								'height' => $large_image_attribute[2]
							),
					);
			}
		}
		?>

		<div class="images gallery_container">
			<div class="product_gallery">
            <?php
			$g_thumb_width = get_option( WOO_DYNAMIC_GALLERY_PREFIX.'thumb_width' );
			if ( $g_thumb_width <= 0 ) {
				$g_thumb_width = 105;
			}
			$g_thumb_height = get_option( WOO_DYNAMIC_GALLERY_PREFIX.'thumb_height' );
			if ( $g_thumb_height <= 0 ) {
				$g_thumb_height = 75;
			}

			$g_auto            = get_option( WOO_DYNAMIC_GALLERY_PREFIX.'product_gallery_auto_start' );
			$g_speed           = get_option( WOO_DYNAMIC_GALLERY_PREFIX.'product_gallery_speed' );
			$g_effect          = get_option( WOO_DYNAMIC_GALLERY_PREFIX.'product_gallery_effect' );
			$g_animation_speed = get_option( WOO_DYNAMIC_GALLERY_PREFIX.'product_gallery_animation_speed' );
			$popup_gallery     = get_option( WOO_DYNAMIC_GALLERY_PREFIX.'popup_gallery' );
			$zoom_label        = __('ZOOM +', 'woo_dgallery');
			if ( 'deactivate' == $popup_gallery ) {
				$zoom_label     = '';
				$lightbox_class = '';
			}

			$_upload_dir = wp_upload_dir();
			if ( file_exists( $_upload_dir['basedir'] . '/sass/woo_dynamic_gallery.min.css' ) ) {
				echo  '<link media="all" type="text/css" href="' . str_replace(array('http:','https:'), '', $_upload_dir['baseurl'] ) . '/sass/woo_dynamic_gallery.min.css?ver='.WOO_DYNAMIC_GALLERY_VERSION.'" rel="stylesheet" />' . "\n";
			} else {
				include( WOO_DYNAMIC_GALLERY_DIR . '/templates/customized_style.php' );
			}

			echo '<script type="text/javascript">
				jQuery(function() {
					var settings_defaults_'.$product_id.' = { loader_image: "'.WOO_DYNAMIC_GALLERY_JS_URL.'/mygallery/loader.gif",
						start_at_index: 0,
						gallery_ID: "'.$product_id.'",
						lightbox_class: "'.$lightbox_class.'",
						description_wrapper: false,
						thumb_opacity: 0.5,
						animate_first_image: false,
						animation_speed: '.$g_animation_speed.'000,
						width: false,
						height: false,
						display_next_and_prev: true,
						display_back_and_forward: true,
						scroll_jump: 0,
						slideshow: {
							enable: true,
							autostart: '.$g_auto.',
							speed: '.$g_speed.'000,
							start_label: "'.__('START SLIDESHOW', 'woo_dgallery').'",
							stop_label: "'.__('STOP SLIDESHOW', 'woo_dgallery').'",
							zoom_label: "'.$zoom_label.'",
							stop_on_scroll: true,
							countdown_prefix: "(",
							countdown_sufix: ")",
							onStart: false,
							onStop: false
						},
						effect: "'.$g_effect.'",
						enable_keyboard_move: true,
						cycle: true,
						callbacks: {
						init: false,
						afterImageVisible: false,
						beforeImageVisible: false
					}
				};
				jQuery("#gallery_'.$product_id.'").adGallery(settings_defaults_'.$product_id.');
			});

			</script>';

			echo '<img style="width: 0px ! important; height: 0px ! important; display: none ! important; position: absolute;" src="'.WOO_DYNAMIC_GALLERY_IMAGES_URL . '/blank.gif">';

			echo '<div id="gallery_'.$product_id.'" class="a3-dgallery">
				<div class="a3dg-image-wrapper"></div>
				<div class="a3dg-controls"> </div>
				<div class="a3dg-nav">
					<div class="a3dg-thumbs">
						<ul class="a3dg-thumb-list">';

						$script_colorbox = '';
						$script_fancybox = '';
						if ( count( $dgallery_ids ) > 0 ) {
							$script_colorbox .= '<script type="text/javascript">';
							$script_fancybox .= '<script type="text/javascript">';
							$script_colorbox .= '(function($){';
							$script_fancybox .= '(function($){';
							$script_colorbox .= '$(function(){';
							$script_fancybox .= '$(function(){';
							$script_colorbox .= '$(document).on("click", ".a3-dgallery .lightbox", function(ev) { if( $(this).attr("rel") == "gallery_'.$product_id.'") {
								var idx = $("#gallery_'.$product_id.' .a3dg-image img").attr("idx");';
							$script_fancybox .= '$(document).on("click", ".a3-dgallery .lightbox", function(ev) { if( $(this).attr("rel") == "gallery_'.$product_id.'") {
								var idx = $("#gallery_'.$product_id.' .a3dg-image img").attr("idx");';

                            if ( count( $dgallery_ids ) <= 1 ) {
								$script_colorbox .= '$(".gallery_product_'.$product_id.'").colorbox({open:true, maxWidth:"100%" });';
								$script_fancybox .= '$.fancybox(';
							} else {
								$script_colorbox .= '$(".gallery_product_'.$product_id.'").colorbox({rel:"gallery_product_'.$product_id.'", maxWidth:"100%" }); $(".gallery_product_'.$product_id.'_"+idx).colorbox({open:true, maxWidth:"100%" });';
								$script_fancybox .= '$.fancybox([';
							}

							$common = '';
							$idx    = 0;

							foreach ( $dgallery_ids as $img_id ) {
								if ( ! isset( $main_dgallery[$img_id] ) ) {
									continue;
								}

								// Get image data from main gallery array
								$gallery_item = $main_dgallery[$img_id];
								$li_class     = '';
								if ( $idx == 0 ) {
									$li_class = 'first_item';
								} elseif ( $idx == count( $dgallery_ids ) - 1 ) {
									$li_class = 'last_item';
								}

								$image_large_url = $gallery_item['large']['url'];
								$image_thumb_url = $gallery_item['thumb']['url'];

								$thumb_height    = $g_thumb_height;
								$thumb_width     = $g_thumb_width;
								$width_old       = $gallery_item['thumb']['width'];
								$height_old      = $gallery_item['thumb']['height'];

								if ( $width_old > $g_thumb_width || $height_old > $g_thumb_height ) {
									if ( $height_old > $g_thumb_height && $g_thumb_height > 0 ) {
										$factor       = ($height_old / $g_thumb_height);
										$thumb_height = $g_thumb_height;
										$thumb_width  = $width_old / $factor;
									}
									if ( $thumb_width > $g_thumb_width && $g_thumb_width > 0 ) {
										$factor       = ($width_old / $g_thumb_width);
										$thumb_height = $height_old / $factor;
										$thumb_width  = $g_thumb_width;
									} elseif ( $thumb_width == $g_thumb_width && $width_old > $g_thumb_width && $g_thumb_width > 0 ) {
										$factor       = ($width_old / $g_thumb_width);
										$thumb_height = $height_old / $factor;
										$thumb_width  = $g_thumb_width;
                                    }
								} else {
									$thumb_height = $height_old;
									$thumb_width = $width_old;
								}

								echo '<li class="'.$li_class.'">';
								echo '<a alt="'. esc_attr( $gallery_item['alt_text'] ).'" class="gallery_product_'.$product_id.' gallery_product_'.$product_id.'_'.$idx.'" title="'. esc_attr( $gallery_item['caption_text'] ).'" rel="gallery_product_'.$product_id.'" href="'.$image_large_url.'">';
								echo '<div><img org-width="'. esc_attr( $gallery_item['large']['width'] ).'" org-height="'. esc_attr( $gallery_item['large']['height'] ).'" sizes="'. esc_attr( $gallery_item['img_sizes'] ) .'" srcset="'. esc_attr( $gallery_item['img_srcset'] ).'" idx="'.$idx.'" style="width:'.$thumb_width.'px !important;height:'.$thumb_height.'px !important" src="'.$image_thumb_url.'" alt="'. esc_attr( $gallery_item['alt_text'] ).'"  data-caption="'. esc_attr( $gallery_item['caption_text'] ).'" class="image'.$idx.'" width="'.$thumb_width.'" height="'.$thumb_height.'"></div>';
								echo '</a>';
								echo '</li>';

                                if ( '' != trim( $gallery_item['caption_text'] ) ) {
									$script_fancybox .= $common.'{href:"'.$image_large_url.'",title:"'.esc_js( $gallery_item['caption_text'] ).'"}';
								} else {
									$script_fancybox .= $common.'{href:"'.$image_large_url.'",title:""}';
								}
								$common = ',';
								$idx++;
							}

							if ( count( $dgallery_ids ) <= 1 ) {
								$script_fancybox .= ');';
							} else {
								$script_fancybox .= '],{
    \'index\': idx
  });';
							}
							$script_colorbox .= 'ev.preventDefault();';
							$script_colorbox .= '} });';
							$script_fancybox .= '} });';
							$script_colorbox .= '});';
							$script_fancybox .= '});';
							$script_colorbox .= '})(jQuery);';
							$script_fancybox .= '})(jQuery);';
							$script_colorbox .= '</script>';
							$script_fancybox .= '</script>';

						} else {
							echo '<li style="width:'.$g_thumb_width.'px;height:'.$g_thumb_height.'px;">';
							echo '<a style="width:'.$g_thumb_width.'px;height:'.$g_thumb_height.'px;" class="" rel="gallery_product_'.$product_id.'" href="'.$no_image_uri.'">';
							echo '<div><img org-width="" org-height="" sizes="" srcset="" style="width:'.$g_thumb_width.'px;height:'.$g_thumb_height.'px;" src="'.$no_image_uri.'" class="image" alt=""></div>';
							echo '</a>';
							echo '</li>';
						}

						if ( 'deactivate' == $popup_gallery ) {
							$script_colorbox = '';
							$script_fancybox = '';
						} elseif ( 'colorbox' == $popup_gallery ) {
							echo $script_colorbox;
						} else {
							echo $script_fancybox;
						}

						echo '</ul>
						</div>
					</div>
				</div>';
			?>
			</div>
		</div>
	<?php
	}
}
?>
