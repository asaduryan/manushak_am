<?php
/**
 * ���� search.php
 *
 * ���������� �������� �����, ����������� �� WordPress
 *
 * @package WordPress
 * @subpackage Simplest_Site
 * @since Simples Site 1.0
 */
?>

<?php get_header();?>

<div id="content">
    <h1>Search result</h1>
    <div class="clear"></div>
    <h1 class="searchRes">in shop</h1>
    <div class="newArrivalsBox">

        <?php
        global $product;
        ?>
        <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
            <?php
            global $post;

            if ($post->post_type == 'product') : ?>

                <div class="shopItem">

                    <div class="shopItemContainer">
                        <div class="productImage">
                            <?php echo $product->get_image(300,220);?>
                        </div>

                        <div class="itemName">
                            <h3>
                                <a href="<?php echo get_permalink( $loop->post->ID ) ?>">
                                    <?php the_title(); ?>
                                </a>
                            </h3>


                        </div>


                    </div>
                    <div class="controlBT" style="">
                        <div class="BTContainer">
                            <div class="CBT"><a href="<?php echo get_permalink( $loop->post->ID ) ?>"><img src="<?php echo get_template_directory_uri().'/img/View.png';?>"></a></div>
                            <div class="CBT"><a href="<?php echo get_permalink( $loop->post->ID ) ?>"><img src="<?php echo get_template_directory_uri().'/img/Wishlist.png';?>"></a></div>
                            <div class="CBT"><a href="<?php echo get_permalink( $loop->post->ID ) ?>"><img src="<?php echo get_template_directory_uri().'/img/Share.png';?>"></a></div>
                            <div class="CBT"><a href="<?php echo get_permalink( $loop->post->ID ) ?>"><img src="<?php echo get_template_directory_uri().'/img/Buy.png';?>"></a></div>
                        </div>
                    </div>
                    <div class="price">
                        <h4>
                            <?php if ( $price_html = $product->get_price_html() ) : ?>
                                <?php echo $price_html; ?>
                            <?php endif; ?>
                        </h4>
                    </div>

                </div>


            <?php endif; ?>

        <?php endwhile; ?>
<!--     a       -->
            <?php else: ?>
<!--      n      -->
        <?php endif; ?>
    </div>
    <div class="newArrivals">
        <h1 class="searchRes">in Posts</h1>
        <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
                <?php if ($post->post_type == 'post'): ?>
                <div class="itemBox">
                    <div class="itemHead">
                        <a href="<?php the_permalink();?>"><h4><?php the_title();?></h4></a> <?php the_date('d.m.Y', '<h5>(', ')</h5>'); ?>
                    </div>
                    <div class="itemContainer"><?php the_content();?></div>
                    <div class="categorys">Category: <?php the_category(', '); ?></div>
                    <?php if(get_the_tags()):?>
                    <div class="tags">Tags: <?php the_tags(' '); ?></div>
                    <?php endif ?>
                            <?php $post_types = get_post_types();
                    ?>
                </div>
                <?php endif; ?>
        <?php endwhile; ?>
            <!--     a       -->
        <?php else: ?>
            <!--      n      -->
        <?php endif; ?>
    </div>
    <div class="smaleBox">
        <div class="smaleBody">
            <div class="smaleItem1"><div><p>text</p><h4>fgagfgafgafgg</h4></div></div>
            <div class="smaleItem2"><div><p>text</p><h4>fgagfgafgafgg</h4></div></div>
            <div class="smaleItem3"><div><p>text</p><h4>fgagfgafgafgg</h4></div></div>
        </div>
    </div>

</div>
<?php get_footer();?>