<?php
/**
 * Файл Index.php
 *
 * Отображает страницы сайта, работающего на WordPress
 *
 * @package WordPress
 * @subpackage Simplest_Site
 * @since Simples Site 1.0
 */
?><!-- CALL HOVERALLS -->

<?php get_header();?>

            <div id="content">
                <?php get_sidebar('home') ?>
                <div class="newArrivals_left">
                    <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
                        <div class="itemBox">
                            <div class="entry-date"><span class="day"><?php echo get_the_date('d'); ?></span><span class="month"><?php echo get_the_date('M'); ?></span></div>
                            <div class="itemHead">
                                <a href="<?php the_permalink();?>"><h4><?php the_title();?></h4></a>
                            </div>
                            <div class="itemContainer"><?php the_content();?></div>
                            <div class="categorys">Category: <?php the_category(', '); ?></div>
                            <?php if(get_the_tags()):?>
                            <div class="tags">Tags: <?php the_tags(' '); ?></div>
                            <?php endif ?>

                        </div>
                    <?php endwhile; ?>
<!--                        --><?php //next_posts_link('Older Entries') ?><!-- --><?php //previous_posts_link('Newer Entries') ?>
                    <?php endif; ?>

                </div>

                <!--                    Pagination-->

                <?php
                $args = array(
                    'show_all'     => False, // показаны все страницы участвующие в пагинации
                    'end_size'     => 1,     // количество страниц на концах
                    'mid_size'     => 1,     // количество страниц вокруг текущей
                    'prev_next'    => True,  // выводить ли боковые ссылки "предыдущая/следующая страница".
                    'prev_text'    => __('« Previous'),
                    'next_text'    => __('Next »'),
                    'add_args'     => False,
                    'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
                    'screen_reader_text' => __( ' ' ),
                );
                the_posts_pagination($args);

                ?>





                <div class="smaleBox">
                    <div class="smaleBody">
                        <div class="smaleItem1"><div><p>text</p><h4>fgagfgafgafgg</h4></div></div>
                        <div class="smaleItem2"><div><p>text</p><h4>fgagfgafgafgg</h4></div></div>
                        <div class="smaleItem3"><div><p>text</p><h4>fgagfgafgafgg</h4></div></div>
                    </div>
                </div>

            </div>

<?php get_footer();?>