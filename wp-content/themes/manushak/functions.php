<?php
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_filter( 'woocommerce_currencies', 'add_my_currency' );

function add_my_currency( $currencies ) {
$currencies['ABC'] = __( 'dram', 'woocommerce' );
return $currencies;
}

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {
switch( $currency ) {
case 'ABC': $currency_symbol = '&#1423;'; break;
}
return $currency_symbol;
}

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
echo '<section id="main">';
    }

    function my_theme_wrapper_end() {
    echo '</section>';
}
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);



function lode_style_script (){
//    wp_enqueue_script('script', get_template_directory_uri().'/js/add-to-cart-variation.js');
    wp_enqueue_script('js', get_template_directory_uri().'/js/js.js');

    wp_enqueue_style('style', get_template_directory_uri().'/'.'style.css');
    wp_enqueue_style('sidebar', get_template_directory_uri().'/layouts/'.'sidebar.css');
    wp_enqueue_style('mediaMax768min479', get_template_directory_uri().'/layouts/'.'mediaMax768min479.css');
    wp_enqueue_style('mediaMax480px', get_template_directory_uri().'/layouts/'.'mediaMax480px.css');
}

add_action('wp_enqueue_scripts','lode_style_script');


//    change script dir
add_action( 'wp_enqueue_scripts', 'load_theme_scripts', 9 );

function load_theme_scripts() {

    wp_enqueue_script( 'wc-add-to-cart', get_template_directory_uri() . '/woocommerce/js/add-to-cart.min.js');
}








add_action( 'wp_footer', 'add_js_to_wp_wcommerce');

function add_js_to_wp_wcommerce(){ ?>
    <script type="text/javascript">

        $( document ).on( 'click', '.removeItem', function(){
            var product_id = jQuery(this).attr("data-product_id");
            var this_page = window.location.toString();
            jQuery.ajax({
                beforeSend: function(){
                },
                type: 'POST',
                dataType: 'json',
                url: "/manushak_am/wp-admin/admin-ajax.php",
                data: { action: "product_remove",
                    product_id: product_id
                }
            }).done (function(){
                // Cart page elements

                $( 'div.shop_table.cart' ).load( this_page + ' .shop_table.cart:eq(0) > *');

                $( '.catMenu.cart_totals' ).load( this_page + ' .catMenu.cart_totals:eq(0) > *');
                if($( 'table .shop_table.cart' )){
                    $( 'table.shop_table.cart' ).load( this_page + ' table.shop_table.cart:eq(0) > *');
                    $( '.cart-collaterals' ).load( this_page + ' .cart-collaterals:eq(0) > *');
                }

            });
            return false;
        });

    </script>
<?php }

add_action( 'wp_ajax_product_remove', 'product_remove' );
add_action( 'wp_ajax_nopriv_product_remove', 'product_remove' );
function product_remove() {
    global  $woocommerce;
    session_start();
    foreach ($woocommerce->cart->get_cart() as $cart_item_key => $cart_item){
        if($cart_item['product_id'] == $_POST['product_id'] ){
            // Remove product in the cart using  cart_item_key.
            $woocommerce->cart->remove_cart_item($cart_item_key);

        }
    }

}
//menu registr
add_action('after_setup_theme', function(){
    register_nav_menus( array(
        'header_menu' => 'header menu',
        'footer_menu' => 'footer menu'
    ) );
});



//top button
function my_scripts_method() {
    wp_enqueue_script(
        'custom-script',
        get_stylesheet_directory_uri() . '/js/topbutton.js',
        array( 'jquery' )
    );
}

add_action( 'wp_enqueue_scripts', 'my_scripts_method' );



//sidebar
if (function_exists('register_sidebars')) register_sidebars(3, array(

    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
    'after_widget'  => '</div>',
    'name'          => 'Sidebar %d'
));


?>

