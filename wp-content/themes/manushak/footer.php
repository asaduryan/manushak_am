<?php
/**
 * Created by PhpStorm.
 * User: Armen
 * Date: 05.10.2015
 * Time: 15:48
 */?>

<a href="#" class="topbutton" style="background-image: url('<?php bloginfo('template_url')?>/img/up.ico')"></a>

<div id="footer">


    <div class="footerBox">
        <div class="footerWidget">
            <div class="footerWidgetBox">
                <div class="footerColum">TEXT WIDGET</div>
                <div class="footerColum">TWITTER WIDGET</div>
                <div class="footerColum">FLICKR WIDGET</div>
            </div>
        </div>
        <div class="footerShareBox">
            <div class="footerShareBody">
                <div class="emailForm"><form action="">
                        <input type="email"><input type="submit" value=">">
                    </form></div>
                <div class="footerShareButons">
                    <div class="share"><img src="../../../../project1/images/1.jpg" alt=""></div>
                    <div class="share"><img src="../../../../project1/images/1.jpg" alt=""></div>
                    <div class="share"><img src="../../../../project1/images/1.jpg" alt=""></div>
                    <div class="share"><img src="../../../../project1/images/1.jpg" alt=""></div>
                    <div class="share"><img src="../../../../project1/images/1.jpg" alt=""></div>
                </div>
            </div>
            <div class="copyRightBox">
                <div class="copyRightContainer">
                    <div class="copyRight">
                        <p>Copyright &copy;

                            <?php
                            $start_year = 2015;
                            echo $start_year;
                            $now_year=date('Y');
                            if(!($now_year==$start_year)){
                                echo '-',$now_year;
                            }

                            ?> manushak.am
                        </p>
                        Designed by Asaduryan.
                    </div>
                    <div class="footerMenu">

                        <?php
                        wp_nav_menu( array(
                            'theme_location'  => 'footer_menu',
                            'menu'            => 'footerMenu',
                            'container'       => '',
                            'container_class' => '',
                            'container_id'    => '',
                            'menu_class'      => 'footer-menu',
                            'menu_id'         => '',
                            'echo'            => true,
                            'fallback_cb'     => 'wp_page_menu',
                            'before'          => '',
                            'after'           => ' /',
                            'link_before'     => '',
                            'link_after'      => '',
                            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'depth'           => 0
                        ) );
                        ?>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

</div>
<?php wp_footer(); ?>

</body>
</html>
