$(document).ready(function(){
    //cart

        $('.catMenu, .userCart').hover(function(){
                $('.userCart').show( "fast").stop(true,true);
            },
            function(){
                $('.userCart').hide( "fast").stop(true,true);
            }
        );



    //menu
    document.querySelector( "#nav-toggle" ).addEventListener( "click", function() {
        this.classList.toggle( "active" );
        $('#menu-menu').toggle().stop(true,true);
    });
    //massages
    $('.man-message').delay(1000).fadeOut(1000);


    $('.specialBlock').hover(function(){
            $(this).find('img').stop(true,true).animate({ opacity: "0.4" }, 500);
        },
        function(){
            $(this).find('img').stop(true,true).animate({ opacity: "1" }, 500);
        }
    );

    //shop hover
    $('.shopItem').live({
        mouseenter: function () {
            $(this).find( ".controlBT" ).stop(true, true).fadeIn(150);
        }
        ,
        mouseleave: function () {
            $(this).find( ".controlBT" ).stop(true, true).fadeOut(150);
        }
    });
    //search box
    $('.searchButton').click(function(){
        $('#searchContainer').slideToggle('slow')
    })
});

window.fbAsyncInit = function() {
    FB.init({
        appId      : '1551008878551056',
        xfbml      : true,
        version    : 'v2.5'
    });
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
