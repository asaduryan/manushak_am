<?php
/**
 * ���� single.php
 *
 * ���������� �������� �����, ����������� �� WordPress
 *
 * @package WordPress
 * @subpackage Simplest_Site
 * @since Simples Site 1.0
 */
?>

<?php get_header();?>
<div id="content">
<!--    --><?php //previous_post_link('%link', $link='? %title') ?><!-- --><?php //next_post_link('%link', $link='%title ? ') ?>

    <div class="clear"></div>
    <div class="newArrivals-single">
        <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
            <div class="itemBox-single">
                <div class="itemHead">
                    <a href="<?php the_permalink();?>"><h4><?php the_title();?></h4></a> <?php the_date('d.m.Y', '<h5>(', ')</h5>'); ?>
                </div>
                <div class="itemContainer"><?php the_content();?></div>
                <div class="categorys">Category: <?php the_category(', '); ?></div>
                <?php if(get_the_tags()):?>
                    <div class="tags">Tags: <?php the_tags(' '); ?></div>
                <?php endif ?>
                <?php comments_template(); ?>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>

    </div>
    <div class="smaleBox">
        <div class="smaleBody">
            <div class="smaleItem1"><div><p>text</p><h4>fgagfgafgafgg</h4></div></div>
            <div class="smaleItem2"><div><p>text</p><h4>fgagfgafgafgg</h4></div></div>
            <div class="smaleItem3"><div><p>text</p><h4>fgagfgafgafgg</h4></div></div>
        </div>
    </div>

</div>
<?php get_footer();?>