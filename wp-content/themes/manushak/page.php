<?php
/**
 * ���� page.php
 *
 * ���������� �������� �����, ����������� �� WordPress
 *
 * @package WordPress
 * @subpackage Simplest_Site
 * @since Simples Site 1.0
 */
?>
<?php get_header(); ?>
    <div id="content">
        <div class="newArrivalsPage">
            <div class="newArrivalsBox">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php ?>
                    <?php the_content(); ?>

                    <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
                <!--    --><?php //comments_template(); ?>
                <?php endwhile; ?>

                <?php else : ?>
                    <h1>Not found</h1>
                    <p> Sorry, but the page you requested does not exist.</p>
                    <?php get_search_form(); ?>

                <?php endif; ?>
            </div>

        </div>
    </div>

<?php get_footer(); ?>