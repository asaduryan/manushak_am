<?php
/**
 * Created by PhpStorm.
 * User: Armen
 * Date: 05.10.2015
 * Time: 15:48
 */?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Rouge+Script' rel='stylesheet' type='text/css'>
    <?php wp_head(); ?>


</head>

<body <?php body_class(); ?>>
<div id="container">
    <header id="masthead" class="site-header" role="banner">
        <div id="header">
            <div class="underHeader">
                <div class="logo">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><p><?php bloginfo( 'name' ); ?></p></a>
                </div>


            </div>
        </div>
        <div id="menu">


             <nav id="primary-navigation" class="site-navigation" role="navigation">
                 <div id="mobileNav"><a id="nav-toggle" href="#"><span></span></a></div>


                 <div class="searchButton"><img src="<?php bloginfo('template_url') ?>/img/search-13-48.ico"></div>

                 <div class="catMenu cart_totals">
                     <img src="<?php bloginfo('template_url') ?>/img/Cart Icon.png" alt="">

                     <span><?php echo sprintf (_n( '%d', '%d', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?></span>

                 </div>

                 <div class="userCart shop_table cart">
                     <?php woocommerce_mini_cart(); ?>

                 </div>
                <?php
                wp_nav_menu( array(
                    'theme_location'  => 'header_menu',
                    'menu'            => 'Menu',
                    'container'       => '',
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => 'nav-menu',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth'           => 0
                ) );
                ?>



            </nav>

        </div>
        <div id="searchContainer">
            <div class="searchBox">
                <?php get_search_form() ?>
            </div>
        </div>
    </header><!-- #masthead -->
    <?php if(is_home()):?>
    <div id="sliderBody">
        <div class="sliderBox"><?php masterslider( '2' ); ?></div>

    </div>


    <div id="specialBox">
        <div class="specialConteiner">
            <div class="specialBlock"><a href=""><img src="<?php bloginfo('template_url') ?>/img/img1.jpg" alt=""><span>ON SALE</span></a></div>
            <div class="specialBlock"><a href=""><img src="<?php bloginfo('template_url') ?>/img/img2.jpg" alt=""><span>SPECIAL OFFERS</span></a></div>
            <div class="specialBlock"><a href=""><img src="<?php bloginfo('template_url') ?>/img/img3.jpg" alt=""><span>MUST HAVE</span></a></div>
        </div>
    </div>
    <?php endif; ?>

