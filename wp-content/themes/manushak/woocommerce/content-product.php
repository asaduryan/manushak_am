<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}
?>
<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
<div class="shopItem">
	<div class="shopItemContainer">
		<a href="<?php echo get_permalink( $loop->post->ID ) ?>">
			<div class="productImage">
			<?php do_action( 'woocommerce_before_shop_loop_item_title' );?>
			</div>
		</a>
		<?php do_action( 'woocommerce_shop_loop_item_title' );?>
	</div>
	<div class="controlBT" style="">
		<div class="BTContainer">
			<div class="CBT"><a href="<?php echo get_permalink( $loop->post->ID ) ?>"><img src="<?php echo get_template_directory_uri().'/img/View.png';?>"></a></div>
			<div class="CBT"><a href="<?php echo get_permalink( $loop->post->ID ) ?>"><img src="<?php echo get_template_directory_uri().'/img/Wishlist.png';?>"></a></div>
			<div class="CBT"><a href="<?php echo get_permalink( $loop->post->ID ) ?>"><img src="<?php echo get_template_directory_uri().'/img/Share.png';?>"></a></div>
			<div class="CBT add_to_cart product_type_simple" data-quantity="1" data-product_id="<?php echo $product->id; ?>"><img src="<?php echo get_template_directory_uri().'/img/Buy.png';?>"></div>
		</div>
	</div>

<?php
do_action( 'woocommerce_after_shop_loop_item_title' );
?>

</div>
