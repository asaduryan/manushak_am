<?php
/**
 * Product loop title
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="itemName">
	<h3>
		<a href="<?php echo get_permalink( $loop->post->ID ) ?>">
			<?php the_title(); ?>
		</a>
	</h3>
</div>
